import { Schema, model } from 'mongoose'

const userSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    level: {
        default: 1,
        type: Number,
        required: false
    },
    email: {
        type: String,
        required: true
    },
    password:{
        type:String,
        required: true
    },
    userId: {
        type: String,
        index:true,
        required: true,
        unique:true
    }
})

export default model('User', userSchema)