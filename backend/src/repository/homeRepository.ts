import homeInterface from './home.model.interface'

export const getMessage=()=>{
    return buildMessage()
}

function buildMessage (): homeInterface {
    return {
        code: 200,
        message: 'Bienvenido a HobbieRoom!'
    }
}