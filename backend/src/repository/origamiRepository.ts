import Origami from './origami.model.interface'

export const insertOrigamiRepository = async (name: string, details: string, steps: string[]) => {
    const origami = new Origami({ name, details, steps })
    await origami.save()

    return {
        successful: true,
        origami
    }
}

export const getOrigamisRepository = async () => {
    const origamis = await Origami.find({}, { _id: 0, __v: 0});
    return {
        successfull: true,
        origamis
    }
}

export const getOrigamiStepRepository = async (fig: string, step: number) => {
    const origami = await Origami.find({ name: fig }, { _id: 0, name: 0, details: 0, __v: 0, steps: { $slice: [step,1] }})
    return {
        successfull: true,
        origami
    }
}
export const getOrigamiRepository = async (fig: string) => {
    const origami = await Origami.find({ name: fig }, { _id: 0, __v: 0, steps: 0})
    return {
        successfull: true,
        origami
    }
}
