import userInterface from './user.model.interface'

export const insertUserInfo=async(name:string,email:string,password:string)=>{
    try{
        const mongoose = require('mongoose');
        const userId = mongoose.Types.ObjectId();
        userId.toString();
        const user= new userInterface({name,email,password,userId});
        await user.save()

        return{
            status:"OK",
            message:"Usuario creado correctamente",
            userId:userId
        }
    }catch(e){
        return{
            status:"NOK",
            message: "El usuario no se ha podido crear, o ya existe un usuario con esos datos."
        }
    }
}

export const deleteUserInfo=async(UserId:string)=>{
        try{
            userInterface.findOneAndRemove({UserId:UserId})
            return {
                status:"OK",
                message:"User deleted correctly"
            }
        }catch(e){
            return{
                status:"NOK",
                message:"There was an error deleting user"
            }
        }
    
    }  
export const getUserInfo=async(email:string,password:string)=>{
    try{
        const user = await userInterface.find({email:email,password:password})
        return{
            status: "OK",
            user:user
        } 

    }catch(e){
        return{
            status: "FAIL",
            message: "Este usuario no ha sido creado"
        }
    }
    
}



