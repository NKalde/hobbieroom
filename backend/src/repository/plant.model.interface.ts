import { Schema, model } from 'mongoose'
//import Owner from './user.model.interface'

const plantSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    days: {
        type: Number
    },
    dayMessage:{
        type: String
    },
    plantId: {
        type: String,
        index:true,
        required: true,
        unique:true
    },
    owner:{
        type:Schema.Types.ObjectId,
        ref: "User",
        autopopulate:true,
        required:true
    }
})

export default model('Plant', plantSchema)