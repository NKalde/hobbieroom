import plantInterface from './plant.model.interface'
import Owner from './user.model.interface'

export const insertPlantInfo=async(name:string,days:number,dayMessage:string,owner:string)=>{
    try{
        const mongoose = require('mongoose');
        const plantId = mongoose.Types.ObjectId();
        plantId.toString();
        const plant= new plantInterface({name,days,dayMessage,plantId,owner});
        await plant.save()

        return{
            status:"OK",
            message:"Plant created correctly",
            plantId:plantId
        }
    }catch(e){
        return{
            status:"NOK",
            message: "There was an error creating a plant"
        }
    }
}

export const getPlantByUser=async(owner_id:string)=>{
    try{
        const owner_plant = await plantInterface.find({owner:owner_id})
        return owner_plant

    }catch(e){
        return{
            status: "FAIL",
            message: "No se pudo mostrar las plantas de ese dueño"
        }
    }
    
}