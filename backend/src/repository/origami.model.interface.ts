import { Schema, model } from 'mongoose'

const origamiSchema = new Schema({
    name: {type: String},
    details: {type: String},
    steps: {
        type: [String]
    }
})

export default model('Origami', origamiSchema)