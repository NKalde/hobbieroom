import express, { Express } from 'express'
import bodyParser from 'body-parser'
import mongoose from 'mongoose'

// Controllers
import * as homeController from './controller/homeController'
import * as plantController from './controller/plantController'
import * as userController from './controller/userController'
import * as origamiController from './controller/origamiController'
const server: Express = express()

server.use(bodyParser.json())
server.use(bodyParser.urlencoded({
    extended: true
}))

mongoose.connect('mongodb://mongo:27017/hobbieroom_db', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true
})

// Rutas Home

server.get('/',homeController.getHelloMessage)

// Rutas de Plantas

server.post('/plants',async(req,res)=>{
    const response = await plantController.insertPlant(req.body.name, req.body.days, req.body.dayMessage,req.body.owner)
    if(response.status=="OK"){
        res.status(200).json(response)
    }else{
        res.status(500).json(response)
    }
})

server.get('/plants/:user_name',async(req,res)=>{
    const response = await plantController.getPlants(req.params.user_name)
    res.status(200).json(response)
})

// Rutas de User
server.post('/user',async(req,res)=>{
    const response = await userController.insertUser(req.body.name, req.body.email, req.body.password)
    if(response.status=="OK"){
        res.status(200).json(response)
    }else{
        res.status(500).json(response)
    }
})

server.get('/user',async (req,res)=>{
    const response = await userController.getUser(req.body.email,req.body.password)
    if(response.status=="OK"){
        res.status(200).json(response)
    }else{
        res.status(200).json(response)
    }
    
})
// Rutas de Origami

server.post('/origami/insert', async (req, res) => {
    const response = await origamiController.insertOrigami(req.body.name, req.body.details, req.body.steps)
    res.json(response)
})

server.get('/origami', async (req, res) => {
    const response = await origamiController.getOrigamis() 
    res.json(response)
})

server.get('/origami/:fig', async (req, res) => {
    const response = await origamiController.getOrigami(req.params.fig)
    res.json(response)
})

server.get('/origami/:fig/:step', async (req, res) => {
    const response = await origamiController.getOrigamiStep(req.params.fig, Number(req.params.step))
    res.json(response)
})


server.listen(3000, () => {
    console.log('Server listening at port 3000')
})