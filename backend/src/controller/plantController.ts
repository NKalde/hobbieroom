import * as Plant from "../repository/plantRepository"

export const insertPlant=(name:string,days:number,dayMessage:string,owner:string)=>{
    return Plant.insertPlantInfo(name,days,dayMessage,owner)
}
export const getPlants=(name:string)=>{
    return Plant.getPlantByUser(name)
}