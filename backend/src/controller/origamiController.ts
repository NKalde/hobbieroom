import * as Origami from '../repository/origamiRepository'

export const insertOrigami = (name: string, details: string, steps: Array<string>) => {
    return Origami.insertOrigamiRepository(name, details, steps)
}

export const getOrigamis = () => {
    return Origami.getOrigamisRepository()
}

export const getOrigamiStep = (fig: string, step: number) => {
    return Origami.getOrigamiStepRepository(fig, step)
}

export const getOrigami = (fig: string) => {
    return Origami.getOrigamiRepository(fig)
}
