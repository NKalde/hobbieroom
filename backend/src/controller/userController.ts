import {insertUserInfo, getUserInfo} from "../repository/userRepository"

export const insertUser=(name:string,email:string,password:string)=>{
    return insertUserInfo(name,email,password)
}
export const getUser=(email:string,password:string)=>{
    return getUserInfo(email,password)
}