import { Request, Response } from 'express'
import * as homeRepository from '../repository/homeRepository'

export const getHelloMessage = (request: Request, response: Response) =>{
    response.json(homeRepository.getMessage())
}