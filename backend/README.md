## Instrucciones de instalación

```
npm install
```

```
npm update
```

### Instalación de Typescript

```
npm install -g typescript
```

### Instalación de @Typesmongoose

```
npm install -g @types/mongoose
```

### Instalación de jest

```
npm install -g jest
```

## Levantando el proyecto

```
docker-compose up -d
```

## Ejecutándo los tests

```
npm run test
```

