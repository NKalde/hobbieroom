import 'bootstrap/dist/css/bootstrap.min.css';
import NavBar from './components/navBar.component'
import Home from './layouts/home.layout'
import Login from './layouts/login.layout'
import Register from './layouts/register.layout'
import Profile from './layouts/profile.layout'
import Origami from './layouts/origami.layout'
import Huerto from './layouts/huerto.layout'
import { BrowserRouter as Router, Route } from 'react-router-dom'


function App() {
  return (
    <Router>
      <div style={{backgroundColor:"#5e87d9"}}>
        <NavBar link="login" text="Log In"/>
        <Route path='/' exact component={Home}/>
        <Route path='/login' component={Login}/>
        <Route path='/register' component={Register}/>
        <Route path='/profile' component={Profile}/>
        <Route path='/huerto' component={Huerto}/>
        <Route path='/origami' component={Origami}/>
      </div>
    </Router>
  );
}

export default App;