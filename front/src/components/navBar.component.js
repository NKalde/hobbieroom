import {Navbar, Form, NavDropdown, FormControl, Button, Nav} from 'react-bootstrap'

function NavBar(props){
    return(
        <div>
            <Navbar bg="light" expand="lg">
            <Navbar.Brand>HobbieRoom</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="mr-auto">
                <Nav.Link href="/">Home</Nav.Link>
                <Nav.Link href="/origami">Origami</Nav.Link>
                <Nav.Link href="/huerto">Huerto</Nav.Link>
                </Nav>
                <Form inline>
                <Button href={"/"+props.link} variant="outline-success">{props.text}</Button>
                </Form>
            </Navbar.Collapse>
            </Navbar>
        </div>
    )
}

export default NavBar;