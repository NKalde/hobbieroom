import { Carousel } from 'react-bootstrap'
import './welcome.css'
import paso1 from'../images/paso1.png'
import paso2 from'../images/paso2.png'
import paso3 from'../images/paso3.png'
import paso4 from'../images/paso4.png'
import paso5 from'../images/paso5.png'
import paso6 from'../images/paso6.png'
import paso7 from'../images/paso7.png'
import paso8 from'../images/paso8.png'
import paso9 from'../images/paso9.png'


function HuertoForm(){
    return(
        <div class="abs-center">
            <Carousel>
                <Carousel.Item>
                    <img
                    width={600} height={600} alt="900x900"
                    className="d-block w-100"
                    src={paso1}
                    />
                    <Carousel.Caption>
                    <p style={{color:"black"}}>1</p>
                    </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item>
                    <img
                    className="d-block w-100"
                    src={paso2}
                    width={600} height={600} alt="900x900"
                    />

                    <Carousel.Caption>
                    <p style={{color:"black"}}>2</p>
                    </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item>
                    <img
                    className="d-block w-100"
                    src={paso3}
                    width={600} height={600} alt="900x900"
                    />

                    <Carousel.Caption>
                    <p style={{color:"black"}}>3</p>
                    </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item>
                    <img
                    className="d-block w-100"
                    src={paso4}
                    width={600} height={600} alt="900x900"
                    />
                    <Carousel.Caption>
                    <p style={{color:"black"}}>4</p>
                    </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item>
                    <img
                    className="d-block w-100"
                    src={paso5}
                    width={600} height={600} alt="900x900"
                    />

                    <Carousel.Caption>
                    <p style={{color:"black"}}>5</p>
                    </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item>
                    <img
                    className="d-block w-100"
                    src={paso6}
                    width={600} height={600} alt="900x900"
                    />

                    <Carousel.Caption>
                    <p style={{color:"black"}}>6</p>
                    </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item>
                    <img
                    className="d-block w-100"
                    src={paso7}
                    width={600} height={600} alt="900x900"
                    />
                    <Carousel.Caption>
                    <p style={{color:"black"}}>7</p>
                    </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item>
                    <img
                    className="d-block w-100"
                    src={paso8}
                    width={600} height={600} alt="900x900"
                    />

                    <Carousel.Caption>
                    <p style={{color:"black"}}>8</p>
                    </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item>
                    <img
                    className="d-block w-100"
                    src={paso9}
                    width={600} height={600} alt="900x900"
                    />

                    <Carousel.Caption>
                    <p style={{color:"black"}}>9</p>
                    </Carousel.Caption>
                </Carousel.Item>
            </Carousel>
        </div>
    )
}
export default HuertoForm;