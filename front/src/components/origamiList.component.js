import {Button, Form, Jumbotron, Carousel } from 'react-bootstrap'
import {BsFillForwardFill} from 'react-icons/bs'
import './loginForm.css'

function OrigamiList (props){ 
    if(props.botones.flag==true){
        return(
                <div class='abs-center'>
                <Jumbotron>
                        <Form>
                            {props.origami.map((ori)=>
                            <Form.Group>
                                <div key={ori.nombre} className="row" style={{marginTop:10}}>
                                    <div className="col-md-6 col-sm-12"><h4>{ori.nombre}</h4><img width="70" height="70" src={ori.image}/></div>
                                        <Button title="GO" variant ="primary" size="sm" onClick={() => props.onAdd({flag:false,oriAux:ori})}>                                       
                                            IR a los pasos
                                            <br></br>     
                                            < BsFillForwardFill/>
                                        </Button>
                                    <div className="col-md-6 col-sm-12">
                                    </div>
                                </div>
                            </Form.Group>
                            )}
                        </Form>
                    </Jumbotron>
                </div>
        )
    }else{
        return(
            <center>
                <br></br>
                <div>
                    <Button title="GO" variant ="light" size="sm" onClick={() => props.onAdd({flag:true,oriAux:[]})}>
                        Volver atras.
                    </Button>
                </div>
                <div class="abs-center">
                    <Carousel>
                        {props.botones.oriAux.images.map((ori)=>
                            <Carousel.Item>
                                <img
                                className="d-block w-100"
                                src={ori.image}
                                alt="First slide"
                                />
                                <Carousel.Caption>
                                <p style={{color:"black"}}>{ori.text}</p>
                                </Carousel.Caption>
                            </Carousel.Item>
                        )}
                    </Carousel>
                </div>
            </center>
        )
    }
}

export default OrigamiList