import {Form, Button, Jumbotron} from 'react-bootstrap'
import './registerForm.css'

function RegisterForm(){
    return(
        <center>
            <div class="abs-center">
                <Jumbotron>
                    <Form>
                        <Form.Group controlId="formBasicEmail">
                            <Form.Label>Name</Form.Label>
                            <Form.Control type="name" placeholder="Enter your Name" />
                        </Form.Group>
                        <Form.Group controlId="formBasicEmail">
                            <Form.Label>Email</Form.Label>
                            <Form.Control type="email" placeholder="Enter email" />
                        </Form.Group>

                        <Form.Group controlId="formBasicPassword">
                            <Form.Label>Password</Form.Label>
                            <Form.Control type="password" placeholder="Password" />
                        </Form.Group>
                        <Button style={{backgroundColor:"#5e87d9"}}variant="primary">
                            Registrar
                        </Button>
                    </Form>
                </Jumbotron>
            </div>
        </center>)
}
export default RegisterForm;