import { Carousel } from 'react-bootstrap'
import './welcome.css'
import home from'../HobbieRoom.png'
import home2 from'../HobbieRoom2.png'
import home3 from'../HobbieRoom3.png'

function Welcome(){
    return(
        <div class="abs-center">
            <Carousel>
                <Carousel.Item>
                    <img
                    className="d-block w-100"
                    src={home}
                    alt="First slide"
                    />
                    <Carousel.Caption>
                    <p style={{color:"black"}}>Un lugar donde puedes manejar tus hobbies, al mismo tiempo que subes nivel en tus habilidades.</p>
                    </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item>
                    <img
                    className="d-block w-100"
                    src={home2}
                    alt="Third slide"
                    />

                    <Carousel.Caption>
                    <p style={{color:"black"}}>Aprende a crear diferentes figuras de Origami.</p>
                    </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item>
                    <img
                    className="d-block w-100"
                    src={home3}
                    alt="Third slide"
                    />

                    <Carousel.Caption>
                    <p style={{color:"black"}}>Y tambien a crear tu propio huerto desde 0. ¡Crea tu cuenta ahora!</p>
                    </Carousel.Caption>
                </Carousel.Item>
            </Carousel>
        </div>
    )
}
export default Welcome