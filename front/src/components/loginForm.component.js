import {Form, Button, Jumbotron, Accordion} from 'react-bootstrap'
import './loginForm.css'
function LoginForm(){
    return(
        <center>
            <div class="abs-center">
                <Jumbotron>
                    <Form>
                        <Form.Group controlId="formBasicEmail">
                            <Form.Label>Email</Form.Label>
                            <Form.Control type="email" placeholder="Enter email" />
                        </Form.Group>

                        <Form.Group controlId="formBasicPassword">
                            <Form.Label>Password</Form.Label>
                            <Form.Control type="password" placeholder="Password" />
                        </Form.Group>
                        <Button style={{backgroundColor:"#5e87d9"}}variant="primary">
                            Ingresar
                        </Button>
                        <div>
                            <br></br>
                            <p>Aun no tienes cuenta en HobbieRoom?</p>
                            <Accordion.Toggle as={Button} href='/register' variant="link" eventKey="0">
                                Registrate!
                            </Accordion.Toggle>
                        </div>
                        
                    </Form>
                </Jumbotron>
            </div>
        </center>)
}
export default LoginForm;