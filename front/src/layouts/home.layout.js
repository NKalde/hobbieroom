import axios from 'axios';
import { useEffect, useState } from 'react';
import NavBar from '../components/navBar.component'
import Welcome from '../components/welcome.component'
import LoginForm from '../components/loginForm.component'

function Home(){
    return(
        <div>
            <Welcome/>
        </div>
    )
}
export default Home;