import OrigamiList from '../components/origamiList.component'
import { useState } from 'react';

function Origami(){
    const [aux, setAuxState] = useState({flag:true,oriAux:{}})
    return(
        <div>
            <OrigamiList origami={buildFakeOrigami()} botones={aux} onAdd={setAuxState}/>
        </div>
    )
}

function buildFakeOrigami(){
    return[
        {
            "nombre":"TheBasic",
            "dificultad":"facil",
            "images":[
                  {"image":"https://480061-1514753-raikfcquaxqncofqfm.stackpathdns.com/data/0/1_s.webp",
                  "text":"Fold the paper in half."},
                  {"image":"https://480061-1514753-raikfcquaxqncofqfm.stackpathdns.com/data/0/2_s.webp",
                  "text":"Unfold and then fold the top two corners into the center line"},
                  {"image":"https://480061-1514753-raikfcquaxqncofqfm.stackpathdns.com/data/0/3_s.webp",
                  "text":"Again, fold the paper in half"},
                  {"image":"https://480061-1514753-raikfcquaxqncofqfm.stackpathdns.com/data/0/4_s.webp","text":"Finally, fold the edges down to meet the bottom of the body."},
              ],
            "image":"https://480061-1514753-raikfcquaxqncofqfm.stackpathdns.com/data/0/square.webp"
        },
        {
            "nombre":"TheSprinter",
            "dificultad":"medio",
            "images":[
                {"image":"https://480061-1514753-raikfcquaxqncofqfm.stackpathdns.com/data/4/1_s.webp",
                "text":"Fold the paper in half diagonally so opposite corners meet."},
                {"image":"https://480061-1514753-raikfcquaxqncofqfm.stackpathdns.com/data/4/2_s.webp",
                "text":"Fold the long edge about 1/2 inch"},
                {"image":"https://480061-1514753-raikfcquaxqncofqfm.stackpathdns.com/data/4/3_s.webp",
                "text":"Fold the paper in half towards you and then rotate so that the thickest point is facing up. The squared off point will be facing to the left"},
                {"image":"https://480061-1514753-raikfcquaxqncofqfm.stackpathdns.com/data/4/4_s.webp:",
                "text":"Fold the right side over as far as it will go and make a vertical crease"},
                {"image":"https://480061-1514753-raikfcquaxqncofqfm.stackpathdns.com/data/4/5_s.webp",
                "text":"Fold the other side down so opposite sides meet"},
                {"image":"https://480061-1514753-raikfcquaxqncofqfm.stackpathdns.com/data/4/6_s.webp",
                "text":"Now, fold one outer flap down to create the first wing. The body will be about 3/4 inch tall."},
                {"image":"https://480061-1514753-raikfcquaxqncofqfm.stackpathdns.com/data/4/7.jpg",
                "text":"Fold the other flap down to complete the plane."}
    
            ],
            "image":"https://480061-1514753-raikfcquaxqncofqfm.stackpathdns.com/data/0/square.webp"
        }
    ]
  }
export default Origami;