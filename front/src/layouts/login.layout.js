import axios from 'axios'
import LoginForm from '../components/loginForm.component'
import { useEffect, useState } from 'react';

function Login(){
    return(
        <div>
            <LoginForm/>
        </div>
    )
}
export default Login;