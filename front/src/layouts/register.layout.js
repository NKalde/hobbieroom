import axios from 'axios'
import RegisterForm from '../components/registerForm.component'
import { useEffect, useState } from 'react';

function Register(){
    return(
        <div>
            <RegisterForm/>
        </div>
    )
}
export default Register;